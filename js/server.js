"use strict";

const fs = require("fs");
const express = require("express");
const app = express();
const PORT = process.env.PORT || 3000;
const bodyParser = require("body-parser");
app.use(bodyParser());

app.get("/", (req, res) => res.sendFile(__dirname + "/index.html"));

app.post("/post", (req, res) => {
  console.log(req.body);
  console.log("postリクエストがきたよ！");

  const base64 = req.body.pad.split(",")[1];
  const decode = new Buffer.from(base64, "base64");
  fs.writeFile("xxx.png", decode, err => {
    if (err) {
      console.log(err);
    } else {
      console.log("saved");
    }
  });
});

app.listen(PORT);
console.log(`listening on *: ${PORT}`);
